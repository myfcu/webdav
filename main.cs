using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        // cli options
        var options = ParseOptions(args);

        // initial values
        var filesid = NewSid(options.Rand);
        var tests = GenerateTests(filesid);
        var results = new Results
        {
            HaveSuccess = false,
            CreatedDir = false,
            Summary = ""
        };

        // Test connection
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Testing DAV connection");
        var dav = new HttpWebDav();
        dav.UserAgent = "DAVClient/0.1";
        dav.Credentials = new NetworkCredential(options.User, options.Pass, options.Realm);
        dav.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true; // Ignore SSL certificate errors

        if (dav.Open(options.Url))
        {
            Console.WriteLine($"OPEN\t\tSUCCEED:\t\t{options.Url}");
        }
        else
        {
            Console.Error.WriteLine($"OPEN\t\tFAIL:\t{options.Url}\t{dav.Message}");
            return;
        }

        if (!string.IsNullOrEmpty(options.UploadFile))
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Uploading file");
            if (PutLocalFile(dav, $"{options.Url}/{options.UploadLoc}", options.UploadFile))
            {
                Console.WriteLine($"Upload succeeded: {options.Url}/{options.UploadLoc}");
            }
            else
            {
                Console.Error.WriteLine($"Upload failed: {dav.Message}");
            }
            return;
        }

        // let user know the sid
        Console.WriteLine("********************************************************");
        Console.WriteLine($"NOTE\tRandom string for this session: {filesid}");

        // Make new directory
        if (options.CreateDir)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Creating directory");
            if (string.IsNullOrEmpty(options.NewDir))
            {
                options.NewDir = $"DavTestDir_{filesid}";
            }
            var newBase = options.Url;
            if (dav.MkCol($"{options.Url}/{options.NewDir}"))
            {
                Console.WriteLine($"MKCOL\t\tSUCCEED:\t\tCreated {options.Url}/{options.NewDir}");
                results.Summary += $"Created: {options.Url}/{options.NewDir}\n";
                newBase = $"{options.Url}/{options.NewDir}";
                results.CreatedDir = true;
            }
            else
            {
                Console.WriteLine("MKCOL\t\tFAIL");
            }

            // close old conn
            dav.Close();

            // reopen new base
            dav = new HttpWebDav();
            dav.UserAgent = "DAVClient/0.1";
            dav.Credentials = new NetworkCredential(options.User, options.Pass, options.Realm);
            dav.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true; // Ignore SSL certificate errors

            if (!dav.Open(newBase))
            {
                Console.Error.WriteLine($"OPEN\t\tFAIL\tFailed to open new base {newBase}");
                return;
            }
        }
        // put test files
        if (!options.Move && !options.Copy)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Sending test files");
            foreach (var type in tests.Keys)
            {
                if (PutFile(dav, $"{options.Url}/{tests[type].Filename}", Encoding.ASCII.GetBytes(tests[type].Content)))
                {
                    Console.WriteLine($"PUT\t{type}\tSUCCEED:\t{options.Url}/{tests[type].Filename}");
                    results.Summary += $"PUT File: {options.Url}/{tests[type].Filename}\n";
                    results.HaveSuccess = true;
                    tests[type].Result = true;
                }
                else
                {
                    Console.WriteLine($"PUT\t{type}\tFAIL");
                }
            }
        }

        // put test files via move
        if (options.Move)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Sending test files (MOVE method)");
            foreach (var type in tests.Keys)
            {
                var oext = GetExt(tests[type].Filename);
                var txtfile = tests[type].Filename.Replace($".{oext}", $"_{oext}.txt");
                var bypassfile = tests[type].Filename.Replace($".{oext}", $".{oext};.txt");

                if (PutFile(dav, $"{options.Url}/{txtfile}", Encoding.ASCII.GetBytes(tests[type].Content)))
                {
                    Console.WriteLine($"PUT\ttxt\tSUCCEED:\t{options.Url}/{txtfile}");

                    // now move it
                    if (MoveFile(dav, $"{options.Url}/{txtfile}", $"{options.Url}/{bypassfile}"))
                    {
                        Console.WriteLine($"MOVE\t{type}\tSUCCEED:\t{options.Url}/{txtfile} => {options.Url}/{bypassfile}");
                        results.Summary += $"MOVE File: {options.Url}/{txtfile} => {options.Url}/{bypassfile}\n";
                        results.HaveSuccess = true;
                        tests[type].Result = true;
                    }
                    else
                    {
                        Console.WriteLine($"MOVE\t{type}\tFAIL:\t{dav.Message}");
                    }
                }
                else
                {
                    Console.WriteLine($"PUT\ttxt\tFAIL:\t{dav.Message}");
                }
            }
        }

        // put test files via copy
        if (options.Copy)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Sending test files (COPY method)");
            foreach (var type in tests.Keys)
            {
                var oext = GetExt(tests[type].Filename);
                var txtfile = tests[type].Filename.Replace($".{oext}", $"_{oext}.txt");
                var bypassfile = tests[type].Filename.Replace($".{oext}", $".{oext};.txt");

                if (PutFile(dav, $"{options.Url}/{txtfile}", Encoding.ASCII.GetBytes(tests[type].Content)))
                {
                    Console.WriteLine($"PUT\ttxt\tSUCCEED:\t{options.Url}/{txtfile}");

                    // now copy it
                    if (CopyFile(dav, $"{options.Url}/{txtfile}", $"{options.Url}/{bypassfile}"))
                    {
                        Console.WriteLine($"COPY\t{type}\tSUCCEED:\t{options.Url}/{txtfile} => {options.Url}/{bypassfile}");
                        results.Summary += $"COPY File: {options.Url}/{txtfile} => {options.Url}/{bypassfile}\n";
                        results.HaveSuccess = true;
                        tests[type].Result = true;
                    }
                    else
                    {
                        Console.WriteLine($"COPY\t{type}\tFAIL:\t{dav.Message}");
                    }
                }
                else
                {
                    Console.WriteLine($"PUT\ttxt\tFAIL:\t{dav.Message}");
                }
            }
        }

        // check if test files exist
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Checking if files exist");
        foreach (var type in tests.Keys)
        {
            if (dav.Exists($"{options.Url}/{tests[type].Filename}"))
            {
                Console.WriteLine($"EXISTS\t{type}\tSUCCEED:\t{options.Url}/{tests[type].Filename}");
                tests[type].Result = true;
            }
            else
            {
                Console.WriteLine($"EXISTS\t{type}\tFAIL");
            }
        }

        // check if bypass files exist
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Checking if bypass files exist");
        foreach (var type in tests.Keys)
        {
            var oext = GetExt(tests[type].Filename);
            var bypassfile = tests[type].Filename.Replace($".{oext}", $".{oext};.txt");
            if (dav.Exists($"{options.Url}/{bypassfile}"))
            {
                Console.WriteLine($"EXISTS\t{type}\tSUCCEED:\t{options.Url}/{bypassfile}");
                tests[type].Result = true;
            }
            else
            {
                Console.WriteLine($"EXISTS\t{type}\tFAIL");
            }
        }

        // check if directory exists, if we created one
        if (results.CreatedDir)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Checking if directory exists");
            if (dav.Exists($"{options.Url}/{options.NewDir}"))
            {
                Console.WriteLine($"EXISTS\t\tSUCCEED:\t\t{options.Url}/{options.NewDir}");
                results.Summary += $"Directory exists: {options.Url}/{options.NewDir}\n";
            }
            else
            {
                Console.WriteLine("EXISTS\\t\\tFAIL");
            }
        }

        // get test files
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Downloading test files");
        foreach (var type in tests.Keys)
        {
            var localfile = $"{type}_{filesid}.{GetExt(tests[type].Filename)}";
            if (GetFile(dav, $"{options.Url}/{tests[type].Filename}", localfile))
            {
                Console.WriteLine($"GET\t{type}\tSUCCEED:\t{options.Url}/{tests[type].Filename} => {localfile}");
                results.Summary += $"GET File: {options.Url}/{tests[type].Filename} => {localfile}\n";
                tests[type].Result = true;
            }
            else
            {
                Console.WriteLine($"GET\t{type}\tFAIL");
            }
        }

        // get test files via bypass
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Downloading test files via bypass");
        foreach (var type in tests.Keys)
        {
            var oext = GetExt(tests[type].Filename);
            var bypassfile = tests[type].Filename
                .Replace($".{oext}", $".{oext};.txt");
            var localfile = $"{type}_bypass_{filesid}.{oext}";
            if (GetFile(dav, $"{options.Url}/{bypassfile}", localfile))
            {
                Console.WriteLine($"GET\t{type}\tSUCCEED:\t{options.Url}/{bypassfile} => {localfile}");
                results.Summary += $"GET File: {options.Url}/{bypassfile} => {localfile}\n";
                tests[type].Result = true;
            }
            else
            {
                Console.WriteLine($"GET\t{type}\tFAIL");
            }
        }

        // delete test files and bypass files
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Deleting test files and bypass files");
        foreach (var type in tests.Keys)
        {
            if (dav.Delete($"{options.Url}/{tests[type].Filename}"))
            {
                Console.WriteLine($"DELETE\t{type}\tSUCCEED:\t{options.Url}/{tests[type].Filename}");
            }
            else
            {
                Console.WriteLine($"DELETE\t{type}\tFAIL");
            }

            var oext = GetExt(tests[type].Filename);
            var bypassfile = tests[type].Filename.Replace($".{oext}", $".{oext};.txt");
            if (dav.Delete($"{options.Url}/{bypassfile}"))
            {
                Console.WriteLine($"DELETE\t{type}\tSUCCEED:\t{options.Url}/{bypassfile}");
            }
            else
            {
                Console.WriteLine($"DELETE\t{type}\tFAIL");
            }
        }

        // delete directory, if we created one
        if (results.CreatedDir)
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine(" Deleting directory");
            if (dav.Delete($"{options.Url}/{options.NewDir}"))
            {
                Console.WriteLine($"DELETE\t\tSUCCEED:\t\t{options.Url}/{options.NewDir}");
                results.Summary += $"Deleted: {options.Url}/{options.NewDir}\n";
            }
            else
            {
                Console.WriteLine("DELETE\\t\\tFAIL");
            }
        }

        // print summary
        Console.WriteLine("********************************************************");
        Console.WriteLine(" Summary");
        Console.WriteLine(results.Summary);

        // exit code
        if (results.HaveSuccess)
        {
            Environment.ExitCode = 0;
        }
        else
        {
            Environment.ExitCode = 1;
        }
    }

    static Options ParseOptions(string[] args)
    {
        var options = new Options();

        var parser = new CommandLine.Parser(config =>
        {
            config.CaseInsensitiveEnumValues = true;
            config.HelpWriter = Console.Error;
        });

        if (!parser.ParseArguments(args, options))
        {
            Environment.Exit(1);
        }

        return options;
    }

    static string NewSid(int length)
    {
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var random = new Random();
        var result = new string(
            Enumerable.Repeat(chars, length)
                      .Select(s => s[random.Next(s.Length)])
                      .ToArray());
        return result;
    }

    static string GetExt(string filename)
    {
        var parts = filename.Split('.');
        return parts[^1];
    }

    static bool PutFile(HttpWebDav dav, string url, byte[] data)
    {
        dav.Headers.Clear();
        dav.Headers.Add("Content-Type", "application/octet-stream");
        dav.Headers.Add("Content-Length", $"{data.Length}");

        using (var stream = new MemoryStream(data))
        {
            return dav.Put(url, stream);
        }
    }

    static bool GetFile(HttpWebDav dav, string url, string localfile)
    {
        dav.Headers.Clear();

        using (var fs = new FileStream(localfile, FileMode.Create))
        {
            return dav.Get(url, fs);
        }
    }

    static bool MoveFile(HttpWebDav dav, string src, string dest)
    {
        dav.Headers.Clear();
        dav.Headers.Add("Destination", dest);

        return dav.Move(src);
    }

    static bool CopyFile(HttpWebDav dav, string src, string dest)
    {
        dav.Headers.Clear();
        dav.Headers.Add("Destination", dest);

        return dav.Copy(src);
    }

    static bool PutLocalFile(HttpWebDav dav, string url, string localfile)
    {
        if (!File.Exists(localfile))
        {
            Console.Error.WriteLine($"Local file not found: {localfile}");
            return false;
        }

        dav.Headers.Clear();
        dav.Headers.Add("Content-Type", "application/octet-stream");
        dav.Headers.Add("Content-Length", $"{new FileInfo(localfile).Length}");

        using (var fs = new FileStream(localfile, FileMode.Open))
        {
            return dav.Put(url, fs);
        }
    }
